mod_sym_t qpsk_table[4]; 
mod_sym_t qam16_table[16]; 
mod_sym_t qam64_table[64];

int16_t kHz75_25PRB[1024]  = {};
int16_t kHz75_6PRB   = {};
int16_t kHz75_15PRB  = {};
int16_t kHz75_50PRB  = {};
int16_t kHz75_75PRB  = {};
int16_t kHz75_100PRB = {};
