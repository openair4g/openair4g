/*
 * dB_routines.h
 *
 *  Created on: Nov 12, 2013
 *      Author: winckel
 */

#ifndef DB_ROUTINES_H_
#define DB_ROUTINES_H_

int16_t dB_fixed_times10(uint32_t x);

#endif /* DB_ROUTINES_H_ */
