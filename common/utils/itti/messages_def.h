// These messages files are mandatory and must always be placed in first position
#include "intertask_messages_def.h"
#include "timer_messages_def.h"

// Dummy file for the generic intertask interface definition. Actual definition should be in top level build directory.
