/*
 * messages_types.h
 *
 *  Created on: Oct 14, 2013
 *      Author: winckel
 */

#ifndef MESSAGES_TYPES_H_
#define MESSAGES_TYPES_H_

#include "intertask_messages_types.h"
#include "timer_messages_types.h"

// Dummy file for the generic intertask interface definition. Actual definition should be in top level build directory.

#endif /* MESSAGES_TYPES_H_ */
