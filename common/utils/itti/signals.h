#ifndef SIGNALS_H_
#define SIGNALS_H_

int signal_mask(void);

int signal_handle(int *end);

#endif /* SIGNALS_H_ */
