#include "types.h"

#ifndef FILE_TYPE_H_
#define FILE_TYPE_H_

int file_type_file_print(types_t *type, int indent, FILE *file);

int file_type_hr_display(types_t *type, int indent);

#endif /* FILE_TYPE_H_ */
