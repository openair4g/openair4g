
 
//IDT DISTRIBUTION PARAMETERS
#define IDT_DIST GAUSSIAN
#define IDT_MIN 2 
#define IDT_MAX 10
#define IDT_STD_DEV 1
#define IDT_LAMBDA 3

//TRANSPORT PROTOCOL
#define TRANS_PROTO TCP
#define IP_V IPV4

//DATA PACKET SIZE DISTRIBUTION PARAMETERS
#define PKTS_SIZE_DIST POISSON   
#define PKTS_SIZE_MIN 17
#define PKTS_SIZE_MAX 1500
#define PKTS_SIZE_STD_DEV 30
#define PKTS_SIZE_LAMBDA 500

//SOCKET MODE
#define DST_PORT 1234;
#define DST_IP "127.0.0.1"
