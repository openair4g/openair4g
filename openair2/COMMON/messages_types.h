/*
 * messages_types.h
 *
 *  Created on: Oct 14, 2013
 *      Author: winckel
 */

#ifndef MESSAGES_TYPES_H_
#define MESSAGES_TYPES_H_

#include "intertask_messages_types.h"
#include "timer_messages_types.h"

#include "phy_messages_types.h"
#include "mac_messages_types.h"
#include "rlc_messages_types.h"
#include "pdcp_messages_types.h"
#include "rrc_messages_types.h"
#include "nas_messages_types.h"
#include "ral_messages_types.h"
#include "s1ap_messages_types.h"
#include "x2ap_messages_types.h"
#include "sctp_messages_types.h"
#include "udp_messages_types.h"

#endif /* MESSAGES_TYPES_H_ */
