The OpenAirInterface software is composed of four different parts: 

openair1: 3GPP LTE Rel-8 PHY layer + PHY RF simulation

openair2: 3GPP LTE Rel-9 RLC/MAC/PDCP and two RRC implementations

openair3: some networking scripts 

openairITS: IEEE 802.11p software modem (all layers)

targets: scripts to compile and lauch different system targets (simulation, emulation and real-time platforms, user-space tools for these platforms (tbd)) 

see README files in these subdirectories for more information

see also https://twiki.eurecom.fr/twiki/bin/view/OpenAirInterface/WebHome

.
