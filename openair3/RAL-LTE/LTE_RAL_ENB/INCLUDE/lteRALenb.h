/*****************************************************************************
 *   Eurecom OpenAirInterface 3
 *    Copyright(c) 2012 Eurecom
 *
 * Source lteRALenb.h
 *
 * Version 0.1
 *
 * Date  11/18/2013
 *
 * Product MIH RAL LTE
 *
 * Subsystem RAL-LTE
 *
 * Authors Michelle Wetterwald, Lionel Gauthier, Frederic Maurel
 *
 * Description Header file to be included by any module that wants to interface with RAL ENB.
 *
 *****************************************************************************/
#ifndef __LTE_RAL_ENB_H__
#define __LTE_RAL_ENB_H__

#include "MIH_C.h"

#include "openair_types.h"
#include "platform_constants.h"
#include "platform_types.h"

#include "lteRALenb_constants.h"
#include "lteRALenb_variables.h"
#include "lteRALenb_action.h"
#include "lteRALenb_main.h"
#include "lteRALenb_mih_msg.h"
#include "lteRALenb_rrc_msg.h"
#include "lteRALenb_parameters.h"
#include "lteRALenb_process.h"
#include "lteRALenb_subscribe.h"
#include "lteRALenb_thresholds.h"
#endif
