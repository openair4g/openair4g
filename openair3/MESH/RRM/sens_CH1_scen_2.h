/** Header file generated with fdesign on Fri Nov  5 17:11:31 2010.**/

#ifndef FD_sens_CH1_scen_2_h_
#define FD_sens_CH1_scen_2_h_

/** Callbacks, globals and object handlers **/


/**** Forms and Objects ****/
typedef struct {
	FL_FORM *sens_CH1_scen_2;
	void *vdata;
	char *cdata;
	long  ldata;
	FL_OBJECT *User_1_sens;
	FL_OBJECT *User_2_sens;
	FL_OBJECT *User_3_sens;
	FL_OBJECT *User_4_sens;
	FL_OBJECT *Cluster_2_sensing;
} FD_sens_CH1_scen_2;

extern FD_sens_CH1_scen_2 * create_form_sens_CH1_scen_2(void);

#endif /* FD_sens_CH1_scen_2_h_ */
