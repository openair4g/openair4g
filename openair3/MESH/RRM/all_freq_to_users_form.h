/** Header file generated with fdesign on Fri Nov  5 18:43:31 2010.**/

#ifndef FD_all_freq_to_users_h_
#define FD_all_freq_to_users_h_

/** Callbacks, globals and object handlers **/


/**** Forms and Objects ****/
typedef struct {
	FL_FORM *all_freq_to_users;
	void *vdata;
	char *cdata;
	long  ldata;
	FL_OBJECT *User_1_channels;
	FL_OBJECT *User_2_channels;
	FL_OBJECT *User_3_channels;
	FL_OBJECT *User_4_channels;
} FD_all_freq_to_users;

extern FD_all_freq_to_users * create_form_all_freq_to_users(void);

#endif /* FD_all_freq_to_users_h_ */
