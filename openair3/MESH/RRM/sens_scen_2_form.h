/** Header file generated with fdesign on Thu Nov  4 18:36:46 2010.**/

#ifndef FD_sens_scen_2_h_
#define FD_sens_scen_2_h_

/** Callbacks, globals and object handlers **/


/**** Forms and Objects ****/
typedef struct {
	FL_FORM *sens_scen_2;
	void *vdata;
	char *cdata;
	long  ldata;
	FL_OBJECT *User_1;
	FL_OBJECT *User_3;
	FL_OBJECT *User_4;
	FL_OBJECT *User_2;
} FD_sens_scen_2;

extern FD_sens_scen_2 * create_form_sens_scen_2(void);

#endif /* FD_sens_scen_2_h_ */
