/** Header file generated with fdesign on Tue Jun  1 09:25:58 2010.**/

#ifndef FD_Secondary_Network_frequencies_h_
#define FD_Secondary_Network_frequencies_h_

/** Callbacks, globals and object handlers **/


/**** Forms and Objects ****/
typedef struct {
	FL_FORM *Secondary_Network_frequencies;
	void *vdata;
	char *cdata;
	long  ldata;
	FL_OBJECT *Selected_frequencies;
} FD_Secondary_Network_frequencies;

extern FD_Secondary_Network_frequencies * create_form_Secondary_Network_frequencies(void);

#endif /* FD_Secondary_Network_frequencies_h_ */
