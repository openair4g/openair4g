/*!
*******************************************************************************

\file    	ch_init.h

\brief   	Fichier d'entete contenant les declarations des types, des defines ,
			et des fonctions relatives a la gestion de la phase d'initialisation 
			du cluster head.

\author  	BURLOT Pascal

\date    	29/08/08

   
\par     Historique:
			$Author$  $Date$  $Revision$
			$Id$
			$Log$

*******************************************************************************
*/

#ifndef CH_INIT_H
#define CH_INIT_H

#ifdef __cplusplus
extern "C" {
#endif

// ---------------------------------------------------------------------------


#ifdef __cplusplus
}
#endif

#endif /* CH_INIT_H */
