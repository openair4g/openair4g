/*!
*******************************************************************************

\file    	rb_mngt.h

\brief   	Fichier d'entete contenant les declarations des types, des defines ,
			et des fonctions relatives a la gestion	des radio bearers par 
			le cluster head.

\author  	BURLOT Pascal

\date    	29/08/08

   
\par     Historique:
			$Author$  $Date$  $Revision$
			$Id$
			$Log$

*******************************************************************************
*/

#ifndef RB_MNGT_H
#define RB_MNGT_H

#ifdef __cplusplus
extern "C" {
#endif

// ---------------------------------------------------------------------------


#ifdef __cplusplus
}
#endif

#endif /* RB_MNGT_H */
