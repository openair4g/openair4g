/*!
*******************************************************************************

\file    	mr_attach.h

\brief   	Fichier d'entete contenant les declarations des types, des defines ,
			et des fonctions relatives a la gestion	de l'attachement d'un Mesh 
			Router a un cluster.

\author  	BURLOT Pascal

\date    	29/08/08

   
\par     Historique:
			$Author$  $Date$  $Revision$
			$Id$
			$Log$

*******************************************************************************
*/

#ifndef MR_ATTACH_H
#define MR_ATTACH_H

#ifdef __cplusplus
extern "C" {
#endif

// ---------------------------------------------------------------------------


#ifdef __cplusplus
}
#endif

#endif /* MR_ATTACH_H */
