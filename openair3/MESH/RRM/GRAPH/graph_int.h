/*!
*******************************************************************************

\file       graph_int.h

\brief      Fichier d'entête 

            Il contient les declarations de type, des defines relatif aux 
            fonctions d'emulation  des interfaces du RRM (Radio Resource 
            Management ).

\author     IACOBELLI Lorenzo

\date       20/04/10

   
\par     Historique:
            $Author$  $Date$  $Revision$
            $Id$
            $Log$

*******************************************************************************
*/


#ifndef GRAPH_INT_H
#define GRAPH_INT_H

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#endif /* GRAPH_INT_H */
